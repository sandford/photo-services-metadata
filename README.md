# Photo Services Metadata
This repository serves as a holding place for University of Delaware Photographic Services metadata policy files; keyword and autocomplete lists, metadata templates, code replacements, and the like. Hopefully this repository will be updated and expanded as useful changes to the various policy files are identified. Most of this data is formatted for use with [Photo Mechanic](https://home.camerabits.com/tour-photo-mechanic/) but may be adaptable to other software.

### Code Replacement Format
Code replacement definitions are structured as two, case-sensitive lines: the mixed-case first line is intended for use in captions while the lowercase second line is for keywording. A single code can be expanded multiple ways - referred to here as "levels" - written as tab-separated text groups (per the [Photo Mechanic Docs](http://wiki.camerabits.com/en/index.php/Speeding_Up_Captioning#Multiple_Code_Replacement)) accessed using the ```\{code}#{level}\``` format. (e.g. ```\MEM#2\``` expands to ```Memorial Hall (MEM / NC32)```) with backslashes (```\```) serving as the code delimiter character.

Most codes have at least 2 replacement levels with the level order determined by how the code is most frequently used:

**Caption Codes ―** First level is the full expansion. Second level is the full expansion plus the code or abbreviation enclosed in parentheses. Optional additional levels for synonyms, combinations, or alternative names.

**Keyword Codes ―** First level is ```code; expansion; synonyms``` for use in keyword fields. Second level is just the expansion of the code without the repeated code or additional synonyms. Optional additional levels for combinations, synonyms, or alternative names.

Keyword expansions follow additional guidelines:

*  Lowercase
*  Semicolon separated
*  Unaccented, ASCII characters
*  Bare minimum of punctuation
*  No ampersands (use "and")


### Formatting Examples
*Example from "ud.txt" :*
```
NSO     New Student Orientation         New Student Orientation (NSO)
nso     nso; new student orientation    new student orientation
```

This form is frequently expanded for building and location codes (among others) by adding additional "natural language" codes so users needn't remember obscure acronyms or codes. To prevent repetition, natural language codes and synonyms target or "nest" the codes of a designated primary entry as their expansion values. Photo Mechanic will automatically expand these nested codes to their target values without additional input from the user.

*Example from "places.txt" :*
```
Memorial	\NC32\     \NC32#2\
memorial	\nc32\     \nc32#2\     \nc32#3\

MEM		\NC32\     \NC32#2\
mem		\nc32\     \nc32#2\     \nc32#3\

NC32	Memorial Hall     Memorial Hall (MEM / NC32)
nc32	mem; memorial hall;     nc32; \nc32\     memorial hall
```


### Conflicting Codes
Codes may have multiple valid expansion in different contexts, leading to possible conflicts (or overlaps) between code replacement files. As an example, the entered replacement code `\CPC\` could refer to the "Center for Political Communication," defined in `cas.txt`, or the "Computing Center" defined in `places.txt` In such circumstances, the basename of the file containing the code is used as a 'namespace prefix' with a colon `:` separating the prefix from the code. The most commonly used of the available expansions is considered the primary expansion, with the un-prefixed (bare) code acting as a synonym for the primary code.

*In 'cas.txt' the un-prefixed code is defined as a synonym targeting the primary expansion :*
```
CAS:CPC     Center for Political Communication          Center for Political Communication (CPC)
CAS:cpc     cpc; center for political communication     center for political communication
CPC     	\CAS:CPC\     \CAS:CPC#2\
cpc     	\cas:cpc\     \cas:cpc#2\
```

*In 'places.txt' only the namespaced codes are defined :*
```
PLACES:CPC    Computing Center          Computing Center (CPC / NE37)
PLACES:CPC    cpc; computing center 	ne37; \ne37\	computing center
```